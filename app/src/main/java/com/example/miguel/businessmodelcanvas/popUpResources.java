package com.example.miguel.businessmodelcanvas;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

/**
 * Created by MIGUEL on 28/02/2018.
 */

public class popUpResources extends Activity {

    ArrayList<String> resources = new ArrayList<>(); //Array de items de channels
    ArrayList<String> canvasResources = new ArrayList<>(); //Array de items de channels en canvas
    EditText resourcesEditTextOne;
    EditText resourcesEditTextTwo;
    EditText resourcesEditTextThree;
    EditText resourcesEditTextFour;

    SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
        setContentView(R.layout.pop_up_key_resources);

        sharedPreferences = this.getSharedPreferences("com.example.miguel.businessmodelcanvas", Context.MODE_PRIVATE);
        resourcesEditTextOne = findViewById(R.id.editTextResources1);
        resourcesEditTextTwo = findViewById(R.id.editTextResources2);
        resourcesEditTextThree = findViewById(R.id.editTextResources3);
        resourcesEditTextFour = findViewById(R.id.editTextResources4);

        popUpMetrics();  //metricas para que la Activity sea pop-up window


        final Button saveResources = findViewById(R.id.saveResources);
        saveResources.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                resources.add(resourcesEditTextOne.getText().toString());
                resources.add(resourcesEditTextTwo.getText().toString());
                resources.add(resourcesEditTextThree.getText().toString());
                resources.add(resourcesEditTextFour.getText().toString());

                ////////////////////////ESTE ES EL BUENO/////////
                try {
                    sharedPreferences.edit().putString("resources", ObjectSerializer.serialize(resources)).apply();  //Shared preferences ya guarda el arreglo y es posible obtenerlo si no se modifica los customerSegments EditTexts//////////
                    Log.i("first array ", resources.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }



               //Esto va en la Activity de classicCanvas para obtener lo que se guardó en el popUp///
                try {
                    canvasResources = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("resources", ObjectSerializer.serialize(new ArrayList<String>())));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Intent intent = new Intent(popUpResources.this, classicCanvas.class);
                startActivity(intent);
            }
        });

        final Button resetResources = findViewById(R.id.resetResources);
        resetResources.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AlertDialog.Builder(popUpResources.this)
                        .setIcon(android.R.drawable.ic_dialog_dialer)
                        .setTitle("Reset Resources?")
                        .setMessage("Are you sure you want to reset all items?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                resources.add(null);
                                resources.add(null);
                                resources.add(null);
                                resources.add(null);

                                ////////////////////////ESTE ES EL BUENO/////////////////////////////////
                                try {
                                    sharedPreferences.edit().putString("resources", ObjectSerializer.serialize(resources)).apply();  //Shared preferences ya guarda el arreglo y es posible obtenerlo si no se modifica los customerSegments EditTexts//////////
                                    Log.i("first array ", resources.toString());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                //Esto va en la Activity de classicCanvas para obtener lo que se guardó en el popUp///
                                try {
                                    canvasResources = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("resources", ObjectSerializer.serialize(new ArrayList<String>())));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                resourcesEditTextOne.setText(null);
                                resourcesEditTextTwo.setText(null);
                                resourcesEditTextThree.setText(null);
                                resourcesEditTextFour.setText(null);

                                Intent intent = new Intent(popUpResources.this, classicCanvas.class);
                                startActivity(intent);   ///ver si se puede hacer sin el intent//
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();
            }
        });

        //Esto va en la Activity de classicCanvas para obtener lo que se guardó en el popUp///
        try {
            canvasResources = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("resources", ObjectSerializer.serialize(new ArrayList<String>())));
        } catch (Exception e) {
            e.printStackTrace();
        }
         ///////////////
        if (canvasResources != null && !canvasResources.isEmpty()){
            if(canvasResources.get(0) != null) {
                resourcesEditTextOne.setText(canvasResources.get(0));
            }
            if(canvasResources.get(1) != null) {
                resourcesEditTextTwo.setText(canvasResources.get(1));
            }
            if(canvasResources.get(2) != null) {
                resourcesEditTextThree.setText(canvasResources.get(2));
            }
            if(canvasResources.get(3) != null) {
                resourcesEditTextFour.setText(canvasResources.get(3));
            }
        }
    }

    public void popUpMetrics(){
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        getWindow().setLayout((int)(width*0.7),(int)(height*0.7));
    }
}

