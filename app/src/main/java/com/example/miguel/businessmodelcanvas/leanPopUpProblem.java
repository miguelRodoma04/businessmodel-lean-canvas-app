package com.example.miguel.businessmodelcanvas;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

/**
 * Created by MIGUEL on 28/02/2018.
 */

public class leanPopUpProblem extends Activity {

    ArrayList<String> partners = new ArrayList<>(); //Array de items de VP
    ArrayList<String> canvasPartners = new ArrayList<>(); //Array de items de VP en canvas
    EditText partnersEditTextOne;
    EditText partnersEditTextTwo;
    EditText partnersEditTextThree;
    EditText partnersEditTextFour;
    EditText partnersEditTextFive;

    SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
        setContentView(R.layout.lean_popup_problem);

        sharedPreferences = this.getSharedPreferences("com.example.miguel.businessmodelcanvas", Context.MODE_PRIVATE);
        partnersEditTextOne = findViewById(R.id.editTextPartners1);
        partnersEditTextTwo = findViewById(R.id.editTextPartners2);
        partnersEditTextThree = findViewById(R.id.editTextPartners3);
        partnersEditTextFour = findViewById(R.id.editTextPartners4);
        partnersEditTextFive = findViewById(R.id.editTextPartners5);

        popUpMetrics();  //metricas para que la Activity sea pop-up window


        Button savePartners = findViewById(R.id.savePartners);
        savePartners.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                partners.add(partnersEditTextOne.getText().toString());
                partners.add(partnersEditTextTwo.getText().toString());
                partners.add(partnersEditTextThree.getText().toString());
                partners.add(partnersEditTextFour.getText().toString());
                partners.add(partnersEditTextFive.getText().toString());

                ////////////////////////ESTE ES EL BUENO/////////////////////////////////
                try {
                    sharedPreferences.edit().putString("leanProblem", ObjectSerializer.serialize(partners)).apply();  //Shared preferences ya guarda el arreglo y es posible obtenerlo si no se modifica los customerSegments EditTexts//////////
                    Log.i("first array ", partners.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }

               //Esto va en la Activity de classicCanvas para obtener lo que se guardó en el popUp///
                try {
                    canvasPartners = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("leanProblem", ObjectSerializer.serialize(new ArrayList<String>())));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Intent intent = new Intent(leanPopUpProblem.this, leanCanvas.class);
                startActivity(intent);
            }
        });

        final Button resetPartners = findViewById(R.id.resetPartners);
        resetPartners.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AlertDialog.Builder(leanPopUpProblem.this)
                        .setIcon(android.R.drawable.ic_dialog_dialer)
                        .setTitle("Reset Key Partners?")
                        .setMessage("Are you sure you want to reset all items?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                partners.add(null);
                                partners.add(null);
                                partners.add(null);
                                partners.add(null);
                                partners.add(null);

                                ////////////////////////ESTE ES EL BUENO/////////////////////////////////
                                try {
                                    sharedPreferences.edit().putString("leanProblem", ObjectSerializer.serialize(partners)).apply();  //Shared preferences ya guarda el arreglo y es posible obtenerlo si no se modifica los customerSegments EditTexts//////////
                                    Log.i("first array ", partners.toString());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                //Esto va en la Activity de classicCanvas para obtener lo que se guardó en el popUp///
                                try {
                                    canvasPartners = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("leanProblem", ObjectSerializer.serialize(new ArrayList<String>())));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                partnersEditTextOne.setText(null);
                                partnersEditTextTwo.setText(null);
                                partnersEditTextThree.setText(null);
                                partnersEditTextFour.setText(null);
                                partnersEditTextFive.setText(null);

                                Intent intent = new Intent(leanPopUpProblem.this, leanCanvas.class);
                                startActivity(intent);   ///ver si se puede hacer sin el intent//
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();
            }
        });

        //Esto va en la Activity de classicCanvas para obtener lo que se guardó en el popUp///
        try {
            canvasPartners = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("leanProblem", ObjectSerializer.serialize(new ArrayList<String>())));
        } catch (Exception e) {
            e.printStackTrace();
        }
         ///////////////
        if (canvasPartners != null && !canvasPartners.isEmpty()){
            if(canvasPartners.get(0) != null) {
                partnersEditTextOne.setText(canvasPartners.get(0));
            }
            if(canvasPartners.get(1) != null) {
                partnersEditTextTwo.setText(canvasPartners.get(1));
            }
            if(canvasPartners.get(2) != null) {
                partnersEditTextThree.setText(canvasPartners.get(2));
            }
            if(canvasPartners.get(3) != null) {
                partnersEditTextFour.setText(canvasPartners.get(3));
            }
            if(canvasPartners.get(4) != null) {
                partnersEditTextFive.setText(canvasPartners.get(4));
            }
        }
    }


    public void popUpMetrics(){
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        getWindow().setLayout((int)(width*0.7),(int)(height*0.7));
    }
}

