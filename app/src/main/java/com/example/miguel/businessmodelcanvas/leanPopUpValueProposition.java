package com.example.miguel.businessmodelcanvas;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

/**
 * Created by MIGUEL on 13/03/2018.
 */

public class leanPopUpValueProposition extends Activity {

    ArrayList<String> valuePropostion = new ArrayList<>(); //Array de items de VP
    ArrayList<String> canvasValueProposition = new ArrayList<>(); //Array de items de VP en canvas
    EditText ValuePropositionEditTextOne;
    EditText ValuePropositionEditTextTwo;
    EditText ValuePropositionEditTextThree;
    EditText ValuePropositionEditTextFour;
    EditText ValuePropositionEditTextFive;

    SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
        setContentView(R.layout.lean_popup_valueproposition);

        sharedPreferences = this.getSharedPreferences("com.example.miguel.businessmodelcanvas", Context.MODE_PRIVATE);
        ValuePropositionEditTextOne = findViewById(R.id.editTextValueProp1);
        ValuePropositionEditTextTwo = findViewById(R.id.editTextValueProp2);
        ValuePropositionEditTextThree = findViewById(R.id.editTextValueProp3);
        ValuePropositionEditTextFour = findViewById(R.id.editTextValueProp4);
        ValuePropositionEditTextFive = findViewById(R.id.editTextValueProp5);

        popUpMetrics();  //metricas para que la Activity sea pop-up window


        final Button saveValueProposition = findViewById(R.id.saveValueProposition);
        saveValueProposition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                valuePropostion.add(ValuePropositionEditTextOne.getText().toString());
                valuePropostion.add(ValuePropositionEditTextTwo.getText().toString());
                valuePropostion.add(ValuePropositionEditTextThree.getText().toString());
                valuePropostion.add(ValuePropositionEditTextFour.getText().toString());
                valuePropostion.add(ValuePropositionEditTextFive.getText().toString());

                ////////////////////////ESTE ES EL BUENO/////////////////////////////////
                try {
                    sharedPreferences.edit().putString("leanValueProposition", ObjectSerializer.serialize(valuePropostion)).apply();  //Shared preferences ya guarda el arreglo y es posible obtenerlo si no se modifica los customerSegments EditTexts//////////
                    Log.i("first array ", valuePropostion.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }



                //Esto va en la Activity de classicCanvas para obtener lo que se guardó en el popUp///
                try {
                    canvasValueProposition = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("leanValueProposition", ObjectSerializer.serialize(new ArrayList<String>())));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Intent intent = new Intent(leanPopUpValueProposition.this, leanCanvas.class);
                startActivity(intent);
            }
        });

        final Button resetValueProposition = findViewById(R.id.resetValueProposition);
        resetValueProposition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AlertDialog.Builder(leanPopUpValueProposition.this)
                        .setIcon(android.R.drawable.ic_dialog_dialer)
                        .setTitle("Reset Value Proposition?")
                        .setMessage("Are you sure you want to reset all items?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                valuePropostion.add(null);
                                valuePropostion.add(null);
                                valuePropostion.add(null);
                                valuePropostion.add(null);
                                valuePropostion.add(null);

                                ////////////////////////ESTE ES EL BUENO/////////////////////////////////
                                try {
                                    sharedPreferences.edit().putString("leanValueProposition", ObjectSerializer.serialize(valuePropostion)).apply();  //Shared preferences ya guarda el arreglo y es posible obtenerlo si no se modifica los customerSegments EditTexts//////////
                                    Log.i("first array ", valuePropostion.toString());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                //Esto va en la Activity de classicCanvas para obtener lo que se guardó en el popUp///
                                try {
                                    canvasValueProposition = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("leanValueProposition", ObjectSerializer.serialize(new ArrayList<String>())));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                ValuePropositionEditTextOne.setText(null);
                                ValuePropositionEditTextTwo.setText(null);
                                ValuePropositionEditTextThree.setText(null);
                                ValuePropositionEditTextFour.setText(null);
                                ValuePropositionEditTextFive.setText(null);

                                Intent intent = new Intent(leanPopUpValueProposition.this, leanCanvas.class);
                                startActivity(intent);   ///ver si se puede hacer sin el intent//
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();
            }
        });

        //Esto va en la Activity de classicCanvas para obtener lo que se guardó en el popUp///
        try {
            canvasValueProposition = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("leanValueProposition", ObjectSerializer.serialize(new ArrayList<String>())));
        } catch (Exception e) {
            e.printStackTrace();
        }
        ///////////////
        if (canvasValueProposition != null && !canvasValueProposition.isEmpty()){
            if(canvasValueProposition.get(0) != null) {
                ValuePropositionEditTextOne.setText(canvasValueProposition.get(0));
            }
            if(canvasValueProposition.get(1) != null) {
                ValuePropositionEditTextTwo.setText(canvasValueProposition.get(1));
            }
            if(canvasValueProposition.get(2) != null) {
                ValuePropositionEditTextThree.setText(canvasValueProposition.get(2));
            }
            if(canvasValueProposition.get(3) != null) {
                ValuePropositionEditTextFour.setText(canvasValueProposition.get(3));
            }
            if(canvasValueProposition.get(4) != null) {
                ValuePropositionEditTextFive.setText(canvasValueProposition.get(4));
            }
        }
    }


    public void popUpMetrics(){
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        getWindow().setLayout((int)(width*0.7),(int)(height*0.7));
    }

}
