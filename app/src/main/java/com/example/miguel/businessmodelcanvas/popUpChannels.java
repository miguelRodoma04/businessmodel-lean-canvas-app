package com.example.miguel.businessmodelcanvas;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

/**
 * Created by MIGUEL on 28/02/2018.
 */

public class popUpChannels extends Activity {

    ArrayList<String> channels = new ArrayList<>(); //Array de items de channels
    ArrayList<String> canvasChannels = new ArrayList<>(); //Array de items de channels en canvas
    EditText channelsEditTextOne;
    EditText channelsEditTextTwo;
    EditText channelsEditTextThree;
    EditText channelsEditTextFour;

    SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
        setContentView(R.layout.pop_up_channels);

        sharedPreferences = this.getSharedPreferences("com.example.miguel.businessmodelcanvas", Context.MODE_PRIVATE);
        channelsEditTextOne = findViewById(R.id.editTextChannels1);
        channelsEditTextTwo = findViewById(R.id.editTextChannels2);
        channelsEditTextThree = findViewById(R.id.editTextChannels3);
        channelsEditTextFour = findViewById(R.id.editTextChannels4);

        popUpMetrics();  //metricas para que la Activity sea pop-up window


        final Button saveChannels = findViewById(R.id.saveChannels);
        saveChannels.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                channels.add(channelsEditTextOne.getText().toString());
                channels.add(channelsEditTextTwo.getText().toString());
                channels.add(channelsEditTextThree.getText().toString());
                channels.add(channelsEditTextFour.getText().toString());

                ////////////////////////ESTE ES EL BUENO/////////////////////////////////
                try {
                    sharedPreferences.edit().putString("channels", ObjectSerializer.serialize(channels)).apply();  //Shared preferences ya guarda el arreglo y es posible obtenerlo si no se modifica los customerSegments EditTexts//////////
                    Log.i("first array ", channels.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }



               //Esto va en la Activity de classicCanvas para obtener lo que se guardó en el popUp///
                try {
                    canvasChannels = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("channels", ObjectSerializer.serialize(new ArrayList<String>())));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Intent intent = new Intent(popUpChannels.this, classicCanvas.class);
                startActivity(intent);
            }
        });

        final Button resetChannels = findViewById(R.id.resetChannels);
        resetChannels.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AlertDialog.Builder(popUpChannels.this)
                        .setIcon(android.R.drawable.ic_dialog_dialer)
                        .setTitle("Reset Channels?")
                        .setMessage("Are you sure you want to reset all items?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                channels.add(null);
                                channels.add(null);
                                channels.add(null);
                                channels.add(null);

                                ////////////////////////ESTE ES EL BUENO/////////////////////////////////
                                try {
                                    sharedPreferences.edit().putString("channels", ObjectSerializer.serialize(channels)).apply();  //Shared preferences ya guarda el arreglo y es posible obtenerlo si no se modifica los customerSegments EditTexts//////////
                                    Log.i("first array ", channels.toString());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                //Esto va en la Activity de classicCanvas para obtener lo que se guardó en el popUp///
                                try {
                                    canvasChannels = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("channels", ObjectSerializer.serialize(new ArrayList<String>())));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                channelsEditTextOne.setText(null);
                                channelsEditTextTwo.setText(null);
                                channelsEditTextThree.setText(null);
                                channelsEditTextFour.setText(null);

                                Intent intent = new Intent(popUpChannels.this, classicCanvas.class);
                                startActivity(intent);   ///ver si se puede hacer sin el intent//
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();
            }
        });

        //Esto va en la Activity de classicCanvas para obtener lo que se guardó en el popUp///
        try {
            canvasChannels = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("channels", ObjectSerializer.serialize(new ArrayList<String>())));
        } catch (Exception e) {
            e.printStackTrace();
        }
         ///////////////
        if (canvasChannels != null && !canvasChannels.isEmpty()){
            if(canvasChannels.get(0) != null) {
                channelsEditTextOne.setText(canvasChannels.get(0));
            }
            if(canvasChannels.get(1) != null) {
                channelsEditTextTwo.setText(canvasChannels.get(1));
            }
            if(canvasChannels.get(2) != null) {
                channelsEditTextThree.setText(canvasChannels.get(2));
            }
            if(canvasChannels.get(3) != null) {
                channelsEditTextFour.setText(canvasChannels.get(3));
            }
        }
    }

    public void popUpMetrics(){
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        getWindow().setLayout((int)(width*0.7),(int)(height*0.7));
    }
}

