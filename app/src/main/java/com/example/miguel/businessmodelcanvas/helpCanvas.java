package com.example.miguel.businessmodelcanvas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class helpCanvas extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_canvas);

        TextView leanCanvasTitle = findViewById(R.id.leanCanvasTitle);
        TextView classicCanvasTitle = findViewById(R.id.classicCanvasTitle);
        TextView customerRelationships = findViewById(R.id.customerRelationshipsText);
        TextView customerDescritpion = findViewById(R.id.customerDescriptionText);
        TextView unfairAdvantage = findViewById(R.id.UnfairAdvantage);
        TextView unfairAdvDescritpion = findViewById(R.id.unfairDescription);
        TextView keyActivitiesText = findViewById(R.id.keyActivitiesText);
        TextView keyActivitiesDescription = findViewById(R.id.keyActivitiesDescription);
        TextView solutionText = findViewById(R.id.solutionText);
        TextView solutionDescription = findViewById(R.id.solutionDescription);
        TextView keyResourcesText = findViewById(R.id.keyResourcesText);  // No he agregado a los if v/////
        TextView keyResourcesDescription = findViewById(R.id.keyResourcesDescription);
        TextView keyMetricsText = findViewById(R.id.keyMetricsText);
        TextView keyMetricsDescription = findViewById(R.id.keyMetricsDescription);
        TextView keyPartnersText = findViewById(R.id.keyPartnersText);
        TextView keyPartnersDescription = findViewById(R.id.keyPartnersDescription);
        TextView problemText = findViewById(R.id.problemText);
        TextView problemDescription = findViewById(R.id.problemDescription);

//         View decorView = getWindow().getDecorView();
//        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION|View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;
//        decorView.setSystemUiVisibility(uiOptions);

        Intent intent = getIntent();
        int x = intent.getIntExtra("valor",0);
        //Toast.makeText(this, "valor de x = " + x, Toast.LENGTH_SHORT).show();  //x ==1 -> LEAN  / x ==2 -> CLASSIC

        //HACER IF DE QUE TEXTOS SE MUESTRAN DEPEDIENDO DEL VALOR DE 'x'//
        if (x == 1){  //Lean Canvas//
            keyResourcesText.setVisibility(View.INVISIBLE);
            keyResourcesDescription.setVisibility(View.INVISIBLE);
            keyMetricsDescription.setVisibility(View.VISIBLE);
            keyMetricsText.setVisibility(View.VISIBLE);
            keyPartnersDescription.setVisibility(View.INVISIBLE);
            keyPartnersText.setVisibility(View.INVISIBLE);
            problemDescription.setVisibility(View.VISIBLE);
            problemText.setVisibility(View.VISIBLE);
            customerRelationships.setVisibility(View.INVISIBLE);
            customerDescritpion.setVisibility(View.INVISIBLE);
            unfairAdvantage.setVisibility(View.VISIBLE);
            unfairAdvDescritpion.setVisibility(View.VISIBLE);
            leanCanvasTitle.setVisibility(View.VISIBLE);
            classicCanvasTitle.setVisibility(View.INVISIBLE);
            keyActivitiesText.setVisibility(View.INVISIBLE);
            keyActivitiesDescription.setVisibility(View.INVISIBLE);
            solutionDescription.setVisibility(View.VISIBLE);
            solutionText.setVisibility(View.VISIBLE);

            Button returnLeanCanvas = findViewById(R.id.returnCanvas);
            returnLeanCanvas.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(helpCanvas.this, leanCanvas.class);
                    startActivity(intent);
                    finish();
                }
            });

        } else if (x == 2){  //Classic Canvas//
            keyResourcesText.setVisibility(View.VISIBLE);
            keyResourcesDescription.setVisibility(View.VISIBLE);
            keyMetricsDescription.setVisibility(View.INVISIBLE);
            keyMetricsText.setVisibility(View.INVISIBLE);
            keyPartnersDescription.setVisibility(View.VISIBLE);
            keyPartnersText.setVisibility(View.VISIBLE);
            problemDescription.setVisibility(View.INVISIBLE);
            problemText.setVisibility(View.INVISIBLE);
            customerRelationships.setVisibility(View.VISIBLE);
            customerDescritpion.setVisibility(View.VISIBLE);
            unfairAdvantage.setVisibility(View.INVISIBLE);
            unfairAdvDescritpion.setVisibility(View.INVISIBLE);
            leanCanvasTitle.setVisibility(View.INVISIBLE);
            classicCanvasTitle.setVisibility(View.VISIBLE);
            keyActivitiesText.setVisibility(View.VISIBLE);
            keyActivitiesDescription.setVisibility(View.VISIBLE);
            solutionDescription.setVisibility(View.INVISIBLE);
            solutionText.setVisibility(View.INVISIBLE);

            Button returnClassicCanvas = findViewById(R.id.returnCanvas);
            returnClassicCanvas.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(helpCanvas.this, classicCanvas.class);
                    startActivity(intent);
                    finish();
                }
            });
        }
    }
}
