package com.example.miguel.businessmodelcanvas;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    EditText projectNameEdit;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedPreferences = this.getSharedPreferences("com.example.miguel.businessmodelcanvas", Context.MODE_PRIVATE);
        projectNameEdit = findViewById(R.id.projectName);
        Button goProject = findViewById(R.id.goProject);

        goProject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String projectName = projectNameEdit.getText().toString();
                sharedPreferences.edit().putString("projectName", projectName ).apply();
                Intent intent = new Intent(MainActivity.this, canvasSelect.class);
                startActivity(intent);
                finish();
            }
        });

        String projectNameGet = sharedPreferences.getString("projectName", "");
        if (projectNameGet != null && !projectNameGet.isEmpty()){
            projectNameEdit.setText(projectNameGet);
        }
    }

    public void onBackPressed() {
        moveTaskToBack(true);
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(1);
    }
}

