package com.example.miguel.businessmodelcanvas;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

public class classicCanvas extends AppCompatActivity {

    int x;
    ArrayList<String> canvasCustomerSegments; //Arreglo donde se guardan items que van en canvas
    ArrayList<String> canvasValueProposition; //Array de los items de VP
    ArrayList<String> canvasChannels;
    ArrayList<String> canvasRelationships;
    ArrayList<String> canvasStreams;
    ArrayList<String> canvasResources;
    ArrayList<String> canvasActivities;
    ArrayList<String> canvasPartners;
    ArrayList<String> canvasCost;
    SharedPreferences sharedPreferences;
    TextView projectName;

    public void helpCanvas (View view){
        x = 2;
        Intent intent = new Intent(classicCanvas.this, helpCanvas.class);
        intent.putExtra("valor", x);
        startActivity(intent);
    }

    public void onBackPressed() {
        Intent intent = new Intent(classicCanvas.this, canvasSelect.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_classic_canvas);

        sharedPreferences = this.getSharedPreferences("com.example.miguel.businessmodelcanvas", Context.MODE_PRIVATE);
        canvasCustomerSegments = new ArrayList<>();
        canvasValueProposition = new ArrayList<>();
        canvasChannels = new ArrayList<>();
        canvasRelationships = new ArrayList<>();
        canvasStreams = new ArrayList<>();
        canvasResources = new ArrayList<>();
        canvasActivities = new ArrayList<>();
        canvasCost = new ArrayList<>();
        canvasPartners = new ArrayList<>();
        projectName = findViewById(R.id.projectName);

//        View decorView = getWindow().getDecorView();
//        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN;
//        decorView.setSystemUiVisibility(uiOptions);

        String projectNameGet = sharedPreferences.getString("projectName", "");
        projectName.setText(projectNameGet);


        ///////////////////////////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////CUSTOMER SEGMENTS////////////////////////////////////////////////////
        Button customerSegment = findViewById(R.id.customerSegment);
        customerSegment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(classicCanvas.this, popUpCustomerSegments.class);
                startActivity(intent);
            }
        });

        TextView canvasCustSegmTextViewOne = findViewById(R.id.canvasCustSegmentTextViewOne);
        TextView canvasCustSegmTextViewTwo = findViewById(R.id.canvasCustSegmentTextViewTwo);
        TextView canvasCustSegmTextViewThree = findViewById(R.id.canvasCustSegmentTextViewThree);
        TextView canvasCustSegmTextViewFour = findViewById(R.id.canvasCustSegmentTextViewFour);
        TextView canvasCustSegmTextViewFive = findViewById(R.id.canvasCustSegmentTextViewFive);
        try {
            canvasCustomerSegments = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("segments", ObjectSerializer.serialize(new ArrayList<String>())));
        } catch (Exception e) {
            e.printStackTrace();
        }
            if (canvasCustomerSegments != null && !canvasCustomerSegments.isEmpty()){
                canvasCustSegmTextViewOne.setText(canvasCustomerSegments.get(0));
                canvasCustSegmTextViewTwo.setText(canvasCustomerSegments.get(1));
                canvasCustSegmTextViewThree.setText(canvasCustomerSegments.get(2));
                canvasCustSegmTextViewFour.setText(canvasCustomerSegments.get(3));
                canvasCustSegmTextViewFive.setText(canvasCustomerSegments.get(4));
            } else {
                canvasCustSegmTextViewOne.setText("Tap to add Customer Segment");
                canvasCustSegmTextViewTwo.setText(" ");
                canvasCustSegmTextViewThree.setText(" ");
                canvasCustSegmTextViewFour.setText(" ");
                canvasCustSegmTextViewFive.setText(" ");
            }
 /////////////////////////////////////CUSTOMER SEGMENTS////////////////////////////////////////////////////////
 //////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////VALUE PROPOSITION////////////////////////////////////////////////////
        Button valueProposition = findViewById(R.id.valueProposition);
        valueProposition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(classicCanvas.this, popUpValueProposition.class);
                startActivity(intent);
            }
        });

        TextView canvasValuePropositionTextViewOne = findViewById(R.id.canvasValuePropositionTextViewOne);
        TextView canvasValuePropositionTextViewTwo = findViewById(R.id.canvasValuePropositionTextViewTwo);
        TextView canvasValuePropositionTextViewThree = findViewById(R.id.canvasValuePropositionTextViewThree);
        TextView canvasValuePropositionTextViewFour = findViewById(R.id.canvasValuePropositionTextViewFour);
        TextView canvasValuePropositionTextViewFive = findViewById(R.id.canvasValuePropositionTextViewFive);
        try {
            canvasValueProposition = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("valueProposition", ObjectSerializer.serialize(new ArrayList<String>())));
        } catch (Exception e) {
            e.printStackTrace();
        }
            if (canvasCustomerSegments != null && !canvasValueProposition.isEmpty()){
                canvasValuePropositionTextViewOne.setText(canvasValueProposition.get(0));
                canvasValuePropositionTextViewTwo.setText(canvasValueProposition.get(1));
                canvasValuePropositionTextViewThree.setText(canvasValueProposition.get(2));
                canvasValuePropositionTextViewFour.setText(canvasValueProposition.get(3));
                canvasValuePropositionTextViewFive.setText(canvasValueProposition.get(4));
            } else {
                canvasValuePropositionTextViewOne.setText("Tap to add Value Proposition");
                canvasValuePropositionTextViewTwo.setText(" ");
                canvasValuePropositionTextViewThree.setText(" ");
                canvasValuePropositionTextViewFour.setText(" ");
                canvasValuePropositionTextViewFive.setText(" ");
            }
////////////////////////////////////VALUE PROPOSITION////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////// CHANNELS ////////////////////////////////////////////////////
        Button channels = findViewById(R.id.channels);
        channels.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(classicCanvas.this, popUpChannels.class);  //cambiar a channels
                startActivity(intent);
            }
        });
        TextView canvasChannelsTextViewOne = findViewById(R.id.canvasChannelsTextViewOne);
        TextView canvasChannelsTextViewTwo = findViewById(R.id.canvasChannelsTextViewTwo);
        TextView canvasChannelsTextViewThree = findViewById(R.id.canvasChannelsTextViewThree);
        TextView canvasChannelsTextViewFour = findViewById(R.id.canvasChannelsTextViewFour);
        try {
            canvasChannels = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("channels", ObjectSerializer.serialize(new ArrayList<String>())));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (canvasChannels != null && !canvasChannels.isEmpty()){
            canvasChannelsTextViewOne.setText(canvasChannels.get(0));
            canvasChannelsTextViewTwo.setText(canvasChannels.get(1));
            canvasChannelsTextViewThree.setText(canvasChannels.get(2));
            canvasChannelsTextViewFour.setText(canvasChannels.get(3));
        } else {
            canvasChannelsTextViewOne.setText("Tap to add Channels");
            canvasChannelsTextViewTwo.setText(" ");
            canvasChannelsTextViewThree.setText("   ");
            canvasChannelsTextViewFour.setText("   ");
        }
///////////////////////////////////////// CHANNELS ////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//        ////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////// CUSTOMER RELATIONSHIPS ////////////////////////////////////////////////////
        Button relationships = findViewById(R.id.customerRelationships);
        relationships.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(classicCanvas.this, popUpRelationships.class);  //cambiar a channels
                startActivity(intent);
            }
        });
        TextView canvasRelationshipsTextViewOne = findViewById(R.id.canvasRelationshipsTextViewOne);
        TextView canvasRelationshipsTextViewTwo = findViewById(R.id.canvasRelationshipsTextViewTwo);
        TextView canvasRelationshipsTextViewThree = findViewById(R.id.canvasRelationshipsTextViewThree);
        TextView canvasRelationshipsTextViewFour = findViewById(R.id.canvasRelationshipsTextViewFour);
        try {
            canvasRelationships = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("relationships", ObjectSerializer.serialize(new ArrayList<String>())));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (canvasRelationships != null && !canvasRelationships.isEmpty()){
            canvasRelationshipsTextViewOne.setText(canvasRelationships.get(0));
            canvasRelationshipsTextViewTwo.setText(canvasRelationships.get(1));
            canvasRelationshipsTextViewThree.setText(canvasRelationships.get(2));
            canvasRelationshipsTextViewFour.setText(canvasRelationships.get(3));
        } else {
            canvasRelationshipsTextViewOne.setText("Tap to add Customer Relationships");
            canvasRelationshipsTextViewTwo.setText(" ");
            canvasRelationshipsTextViewThree.setText(" ");
            canvasRelationshipsTextViewFour.setText(" ");
        }
///////////////////////////////////////// CUSTOMER RELATIONSHIPS  ////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//        ////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////// REVENUE STREAMS ////////////////////////////////////////////////////
        Button revenueStream = findViewById(R.id.revenueStream);
        revenueStream.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(classicCanvas.this, popUpStreams.class);  //cambiar a channels
                startActivity(intent);
            }
        });
        TextView canvasStreamsTextViewOne = findViewById(R.id.canvasStreamsTextViewOne);
        TextView canvasStreamsTextViewTwo = findViewById(R.id.canvasStreamsTextViewTwo);
        TextView canvasStreamsTextViewThree = findViewById(R.id.canvasStreamsTextViewThree);
        TextView canvasStreamsTextViewFour = findViewById(R.id.canvasStreamsTextViewFour);
        try {
            canvasStreams = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("revenue", ObjectSerializer.serialize(new ArrayList<String>())));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (canvasStreams != null && !canvasStreams.isEmpty()){
            canvasStreamsTextViewOne.setText(canvasStreams.get(0));
            canvasStreamsTextViewTwo.setText(canvasStreams.get(1));
            canvasStreamsTextViewThree.setText(canvasStreams.get(2));
            canvasStreamsTextViewFour.setText(canvasStreams.get(3));
        }
      else {
            canvasStreamsTextViewOne.setText("Tap to add Revenue Streams");
            canvasStreamsTextViewTwo.setText(" ");
            canvasStreamsTextViewThree.setText(" ");
            canvasStreamsTextViewFour.setText(" ");
        }
///////////////////////////////////////// REVENUE STREAMS ////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////// KEY RESOURCES ////////////////////////////////////////////////////
        Button keyResources = findViewById(R.id.keyResources);
        keyResources.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(classicCanvas.this, popUpResources.class);
                startActivity(intent);
            }
        });
        TextView canvasResourcesTextViewOne = findViewById(R.id.canvasResourcesTextViewOne);
        TextView canvasResourcesTextViewTwo = findViewById(R.id.canvasResourcesTextViewTwo);
        TextView canvasResourcesTextViewThree = findViewById(R.id.canvasResourcesTextViewThree);
        TextView canvasResourcesTextViewFour = findViewById(R.id.canvasResourcesTextViewFour);
        try {
            canvasResources = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("resources", ObjectSerializer.serialize(new ArrayList<String>())));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (canvasResources != null && !canvasResources.isEmpty()){
            canvasResourcesTextViewOne.setText(canvasResources.get(0));
            canvasResourcesTextViewTwo.setText(canvasResources.get(1));
            canvasResourcesTextViewThree.setText(canvasResources.get(2));
            canvasResourcesTextViewFour.setText(canvasResources.get(3));
        }
        else {
            canvasResourcesTextViewOne.setText("Tap to add Key Resources");
            canvasResourcesTextViewTwo.setText(" ");
            canvasResourcesTextViewThree.setText(" ");
            canvasResourcesTextViewFour.setText(" ");
        }
///////////////////////////////////////// KEY RESOURCES ////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////// KEY ACTIVITIES ////////////////////////////////////////////////////
        Button keyActivities = findViewById(R.id.keyActivities);
        keyActivities.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(classicCanvas.this, popUpActivities.class);
                startActivity(intent);
            }
        });
        TextView canvasActivitiesTextViewOne = findViewById(R.id.canvasActivitiesTextViewOne);
        TextView canvasActivitiesTextViewTwo = findViewById(R.id.canvasActivitiesTextViewTwo);
        TextView canvasActivitiesTextViewThree = findViewById(R.id.canvasActivitiesTextViewThree);
        TextView canvasActivitiesTextViewFour = findViewById(R.id.canvasActivitiesTextViewFour);
        try {
            canvasActivities = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("activities", ObjectSerializer.serialize(new ArrayList<String>())));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (canvasActivities != null && !canvasActivities.isEmpty()){
            canvasActivitiesTextViewOne.setText(canvasActivities.get(0));
            canvasActivitiesTextViewTwo.setText(canvasActivities.get(1));
            canvasActivitiesTextViewThree.setText(canvasActivities.get(2));
            canvasActivitiesTextViewFour.setText(canvasActivities.get(3));
        }
        else {
            canvasActivitiesTextViewOne.setText("Tap to add Key Activities");
            canvasActivitiesTextViewTwo.setText(" ");
            canvasActivitiesTextViewThree.setText(" ");
            canvasActivitiesTextViewFour.setText(" ");
        }
///////////////////////////////////////// KEY ACTIVITIES ////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////KEY PARTNERS////////////////////////////////////////////////////
        Button keyPartners = findViewById(R.id.keyPartners);
        keyPartners.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(classicCanvas.this, popUpPartners.class);
                startActivity(intent);
            }
        });

        TextView canvasPartnersTextViewOne = findViewById(R.id.canvasPartnersTextViewOne);
        TextView canvasPartnersTextViewTwo = findViewById(R.id.canvasPartnersTextViewTwo);
        TextView canvasPartnersTextViewThree = findViewById(R.id.canvasPartnersTextViewThree);
        TextView canvasPartnersTextViewFour = findViewById(R.id.canvasPartnersTextViewFour);
        TextView canvasPartnersTextViewFive = findViewById(R.id.canvasPartnersTextViewFive);
        try {
            canvasPartners = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("keyPartn", ObjectSerializer.serialize(new ArrayList<String>())));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (canvasPartners != null && !canvasPartners.isEmpty()){
            canvasPartnersTextViewOne.setText(canvasPartners.get(0));
            canvasPartnersTextViewTwo.setText(canvasPartners.get(1));
            canvasPartnersTextViewThree.setText(canvasPartners.get(2));
            canvasPartnersTextViewFour.setText(canvasPartners.get(3));
            canvasPartnersTextViewFive.setText(canvasPartners.get(4));
        } else {
            canvasPartnersTextViewOne.setText("Tap to add Key Partners");
            canvasPartnersTextViewTwo.setText(" ");
            canvasPartnersTextViewThree.setText(" ");
            canvasPartnersTextViewFour.setText(" ");
            canvasPartnersTextViewFive.setText(" ");
        }
////////////////////////////////////KEY PARTNERS////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //        ////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////COST STRUCTURE ////////////////////////////////////////////////////
        Button costStructure = findViewById(R.id.costStructure);
        costStructure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(classicCanvas.this, popUpCost.class);  //cambiar a channels
                startActivity(intent);
            }
        });
        TextView canvasCostTextViewOne = findViewById(R.id.canvasCostTextViewOne);
        TextView canvasCostTextViewTwo = findViewById(R.id.canvasCostTextViewTwo);
        TextView canvasCostTextViewThree = findViewById(R.id.canvasCostTextViewThree);
        TextView canvasCostTextViewFour = findViewById(R.id.canvasCostTextViewFour);
        try {
            canvasCost = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("cost", ObjectSerializer.serialize(new ArrayList<String>())));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (canvasStreams != null && !canvasCost.isEmpty()){
            canvasCostTextViewOne.setText(canvasCost.get(0));
            canvasCostTextViewTwo.setText(canvasCost.get(1));
            canvasCostTextViewThree.setText(canvasCost.get(2));
            canvasCostTextViewFour.setText(canvasCost.get(3));
        }
        else {
            canvasCostTextViewOne.setText("Tap to add Cost Structure");
            canvasCostTextViewTwo.setText(" ");
            canvasCostTextViewThree.setText(" ");
            canvasCostTextViewFour.setText(" ");
        }
///////////////////////////////////////// COST STRUTURE////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    }
}

