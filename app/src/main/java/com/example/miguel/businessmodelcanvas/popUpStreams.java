package com.example.miguel.businessmodelcanvas;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

/**
 * Created by MIGUEL on 28/02/2018.
 */

public class popUpStreams extends Activity {

    ArrayList<String> streams = new ArrayList<>(); //Array de items de channels
    ArrayList<String> canvasStreams = new ArrayList<>(); //Array de items de channels en canvas
    EditText streamsEditTextOne;
    EditText streamsEditTextTwo;
    EditText streamsEditTextThree;
    EditText streamsEditTextFour;

    SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
        setContentView(R.layout.pop_up_revenue_streams);

        sharedPreferences = this.getSharedPreferences("com.example.miguel.businessmodelcanvas", Context.MODE_PRIVATE);
        streamsEditTextOne = findViewById(R.id.editTextStreams1);
        streamsEditTextTwo = findViewById(R.id.editTextStreams2);
        streamsEditTextThree = findViewById(R.id.editTextStreams3);
        streamsEditTextFour = findViewById(R.id.editTextStreams4);

        popUpMetrics();  //metricas para que la Activity sea pop-up window


        final Button saveChannels = findViewById(R.id.saveStreams);
        saveChannels.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                streams.add(streamsEditTextOne.getText().toString());
                streams.add(streamsEditTextTwo.getText().toString());
                streams.add(streamsEditTextThree.getText().toString());
                streams.add(streamsEditTextFour.getText().toString());

                ////////////////////////ESTE ES EL BUENO/////////////////////////////////
                try {
                    sharedPreferences.edit().putString("revenue", ObjectSerializer.serialize(streams)).apply();  //Shared preferences ya guarda el arreglo y es posible obtenerlo si no se modifica los customerSegments EditTexts//////////
                    Log.i("first array ", streams.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }



               //Esto va en la Activity de classicCanvas para obtener lo que se guardó en el popUp///
                try {
                    canvasStreams = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("revenue", ObjectSerializer.serialize(new ArrayList<String>())));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Intent intent = new Intent(popUpStreams.this, classicCanvas.class);
                startActivity(intent);
            }
        });

        final Button resetChannels = findViewById(R.id.resetStreams);
        resetChannels.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AlertDialog.Builder(popUpStreams.this)
                        .setIcon(android.R.drawable.ic_dialog_dialer)
                        .setTitle("Reset Revenue Streams?")
                        .setMessage("Are you sure you want to reset all items?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                streams.add(null);
                                streams.add(null);
                                streams.add(null);
                                streams.add(null);

                                ////////////////////////ESTE ES EL BUENO/////////////////////////////////
                                try {
                                    sharedPreferences.edit().putString("revenue", ObjectSerializer.serialize(streams)).apply();  //Shared preferences ya guarda el arreglo y es posible obtenerlo si no se modifica los customerSegments EditTexts//////////
                                    Log.i("first array ", streams.toString());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                //Esto va en la Activity de classicCanvas para obtener lo que se guardó en el popUp///
                                try {
                                    canvasStreams = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("revenue", ObjectSerializer.serialize(new ArrayList<String>())));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                streamsEditTextOne.setText(null);
                                streamsEditTextTwo.setText(null);
                                streamsEditTextThree.setText(null);
                                streamsEditTextFour.setText(null);

                                Intent intent = new Intent(popUpStreams.this, classicCanvas.class);
                                startActivity(intent);   ///ver si se puede hacer sin el intent//
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();
            }
        });

        //Esto va en la Activity de classicCanvas para obtener lo que se guardó en el popUp///
        try {
            canvasStreams = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("revenue", ObjectSerializer.serialize(new ArrayList<String>())));
        } catch (Exception e) {
            e.printStackTrace();
        }
         ///////////////
        if (canvasStreams != null && !canvasStreams.isEmpty()){
            if(canvasStreams.get(0) != null) {
                streamsEditTextOne.setText(canvasStreams.get(0));
            }
            if(canvasStreams.get(1) != null) {
                streamsEditTextTwo.setText(canvasStreams.get(1));
            }
            if(canvasStreams.get(2) != null) {
                streamsEditTextThree.setText(canvasStreams.get(2));
            }
            if(canvasStreams.get(3) != null) {
                streamsEditTextFour.setText(canvasStreams.get(3));
            }
        }
    }

    public void popUpMetrics(){
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        getWindow().setLayout((int)(width*0.7),(int)(height*0.7));
    }
}

