package com.example.miguel.businessmodelcanvas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class canvasSelect extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_canvas_select);
    }

    public void businessModelCanvas (View view){
        Intent intent = new Intent(canvasSelect.this, classicCanvas.class);
        startActivity(intent);

    }

    public void leanCanvas (View view){
        Intent intent = new Intent(canvasSelect.this, leanCanvas.class);
        startActivity(intent);
    }

    public void whatsDifferent (View view){
        Intent intent =new Intent (canvasSelect.this, whatsDifferent.class);
        startActivity(intent);
    }

    public void onBackPressed() {
        Intent intent = new Intent(canvasSelect.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
