package com.example.miguel.businessmodelcanvas;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

public class leanCanvas extends AppCompatActivity {

    int x;
    ArrayList<String> canvasCustomerSegmentsLean;
    ArrayList<String> canvasValuePropositionLean;
    ArrayList<String> canvasProblemLean;
    ArrayList<String> canvasChannelsLean;
    ArrayList<String> canvasKeyMetricsLean;
    ArrayList<String> canvasSolutionLean;
    ArrayList<String> canvasCostStructureLean;
    ArrayList<String> canvasRevenueStreamLean;
    ArrayList<String> canvasUnfariAdvantageLean;
    SharedPreferences sharedPreferences;
    TextView projectNameLean;

    public void helpLeanCanvas (View view){
        x = 1;
        Intent intent = new Intent(leanCanvas.this, helpCanvas.class);
        intent.putExtra("valor", x);
        startActivity(intent);
    }

    public void onBackPressed(){
        Intent intent = new Intent(leanCanvas.this, canvasSelect.class);
        startActivity(intent);
        finish();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lean_canvas);

        sharedPreferences = this.getSharedPreferences("com.example.miguel.businessmodelcanvas", Context.MODE_PRIVATE);
        canvasCustomerSegmentsLean = new ArrayList<>();
        projectNameLean = findViewById(R.id.projectNameLean);

        String projectNameGet = sharedPreferences.getString("projectName", "");
        projectNameLean.setText(projectNameGet);

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////CUSTOMER SEGMENTS////////////////////////////////////////////////////
        Button leanCustomerSegment = findViewById(R.id.leanCustomerSegment);
        leanCustomerSegment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(leanCanvas.this, leanPopUpSegments.class);
                startActivity(intent);
            }
        });

        TextView canvasCustSegmTextViewOne = findViewById(R.id.canvasCustSegmentTextViewOne);
        TextView canvasCustSegmTextViewTwo = findViewById(R.id.canvasCustSegmentTextViewTwo);
        TextView canvasCustSegmTextViewThree = findViewById(R.id.canvasCustSegmentTextViewThree);
        TextView canvasCustSegmTextViewFour = findViewById(R.id.canvasCustSegmentTextViewFour);
        TextView canvasCustSegmTextViewFive = findViewById(R.id.canvasCustSegmentTextViewFive);
        try {
            canvasCustomerSegmentsLean = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("leanSegments", ObjectSerializer.serialize(new ArrayList<String>())));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (canvasCustomerSegmentsLean != null && !canvasCustomerSegmentsLean.isEmpty()){
            canvasCustSegmTextViewOne.setText(canvasCustomerSegmentsLean.get(0));
            canvasCustSegmTextViewTwo.setText(canvasCustomerSegmentsLean.get(1));
            canvasCustSegmTextViewThree.setText(canvasCustomerSegmentsLean.get(2));
            canvasCustSegmTextViewFour.setText(canvasCustomerSegmentsLean.get(3));
            canvasCustSegmTextViewFive.setText(canvasCustomerSegmentsLean.get(4));
        } else {
            canvasCustSegmTextViewOne.setText("Tap to add Customer Segment");
            canvasCustSegmTextViewTwo.setText(" ");
            canvasCustSegmTextViewThree.setText(" ");
            canvasCustSegmTextViewFour.setText(" ");
            canvasCustSegmTextViewFive.setText(" ");
        }
        /////////////////////////////////////CUSTOMER SEGMENTS////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////VALUE PROPOSITION////////////////////////////////////////////////////
        Button LeanValueProposition = findViewById(R.id.LeanValueProposition);
        LeanValueProposition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(leanCanvas.this, leanPopUpValueProposition.class);
                startActivity(intent);
            }
        });

        TextView canvasValuePropositionTextViewOne = findViewById(R.id.canvasValuePropositionTextViewOne);
        TextView canvasValuePropositionTextViewTwo = findViewById(R.id.canvasValuePropositionTextViewTwo);
        TextView canvasValuePropositionTextViewThree = findViewById(R.id.canvasValuePropositionTextViewThree);
        TextView canvasValuePropositionTextViewFour = findViewById(R.id.canvasValuePropositionTextViewFour);
        TextView canvasValuePropositionTextViewFive = findViewById(R.id.canvasValuePropositionTextViewFive);
        try {
            canvasValuePropositionLean = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("leanValueProposition", ObjectSerializer.serialize(new ArrayList<String>())));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (canvasValuePropositionLean != null && !canvasValuePropositionLean.isEmpty()){
            canvasValuePropositionTextViewOne.setText(canvasValuePropositionLean.get(0));
            canvasValuePropositionTextViewTwo.setText(canvasValuePropositionLean.get(1));
            canvasValuePropositionTextViewThree.setText(canvasValuePropositionLean.get(2));
            canvasValuePropositionTextViewFour.setText(canvasValuePropositionLean.get(3));
            canvasValuePropositionTextViewFive.setText(canvasValuePropositionLean.get(4));
        } else {
            canvasValuePropositionTextViewOne.setText("Tap to add Value Proposition");
            canvasValuePropositionTextViewTwo.setText(" ");
            canvasValuePropositionTextViewThree.setText(" ");
            canvasValuePropositionTextViewFour.setText(" ");
            canvasValuePropositionTextViewFive.setText(" ");
        }
////////////////////////////////////VALUE PROPOSITION////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //        ////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////// UNFARIR ADVANTAGE ////////////////////////////////////////////////////
        Button unfairAdvantage = findViewById(R.id.unfairAdvantage);
        unfairAdvantage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(leanCanvas.this, leanPopUpUnfairAdvantage.class);  //cambiar a channels
                startActivity(intent);
            }
        });
        TextView canvasRelationshipsTextViewOne = findViewById(R.id.canvasRelationshipsTextViewOne);
        TextView canvasRelationshipsTextViewTwo = findViewById(R.id.canvasRelationshipsTextViewTwo);
        TextView canvasRelationshipsTextViewThree = findViewById(R.id.canvasRelationshipsTextViewThree);
        TextView canvasRelationshipsTextViewFour = findViewById(R.id.canvasRelationshipsTextViewFour);
        try {
            canvasUnfariAdvantageLean = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("unfairAdvantage", ObjectSerializer.serialize(new ArrayList<String>())));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (canvasUnfariAdvantageLean != null && !canvasUnfariAdvantageLean.isEmpty()){
            canvasRelationshipsTextViewOne.setText(canvasUnfariAdvantageLean.get(0));
            canvasRelationshipsTextViewTwo.setText(canvasUnfariAdvantageLean.get(1));
            canvasRelationshipsTextViewThree.setText(canvasUnfariAdvantageLean.get(2));
            canvasRelationshipsTextViewFour.setText(canvasUnfariAdvantageLean.get(3));
        } else {
            canvasRelationshipsTextViewOne.setText("Tap to add Unfair Advantage");
            canvasRelationshipsTextViewTwo.setText(" ");
            canvasRelationshipsTextViewThree.setText(" ");
            canvasRelationshipsTextViewFour.setText(" ");
        }
///////////////////////////////////////// UNFAIR ADVANTAGE  ////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////// CHANNELS ////////////////////////////////////////////////////
        Button leanChannels = findViewById(R.id.leanChannels);
        leanChannels.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(leanCanvas.this, leanPopUpChannels.class);  //cambiar a channels
                startActivity(intent);
            }
        });
        TextView canvasChannelsTextViewOne = findViewById(R.id.canvasChannelsTextViewOne);
        TextView canvasChannelsTextViewTwo = findViewById(R.id.canvasChannelsTextViewTwo);
        TextView canvasChannelsTextViewThree = findViewById(R.id.canvasChannelsTextViewThree);
        TextView canvasChannelsTextViewFour = findViewById(R.id.canvasChannelsTextViewFour);
        try {
            canvasChannelsLean = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("leanChannels", ObjectSerializer.serialize(new ArrayList<String>())));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (canvasChannelsLean != null && !canvasChannelsLean.isEmpty()){
            canvasChannelsTextViewOne.setText(canvasChannelsLean.get(0));
            canvasChannelsTextViewTwo.setText(canvasChannelsLean.get(1));
            canvasChannelsTextViewThree.setText(canvasChannelsLean.get(2));
            canvasChannelsTextViewFour.setText(canvasChannelsLean.get(3));
        } else {
            canvasChannelsTextViewOne.setText("Tap to add Channels");
            canvasChannelsTextViewTwo.setText(" ");
            canvasChannelsTextViewThree.setText("   ");
            canvasChannelsTextViewFour.setText("   ");
        }
///////////////////////////////////////// CHANNELS ////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //        ////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////// REVENUE STREAMS ////////////////////////////////////////////////////
        Button leanRevenueStreams = findViewById(R.id.leanRevenueStreams);
        leanRevenueStreams.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(leanCanvas.this, leanPopUpRevenueStreams.class);  //cambiar a channels
                startActivity(intent);
            }
        });
        TextView canvasStreamsTextViewOne = findViewById(R.id.canvasStreamsTextViewOne);
        TextView canvasStreamsTextViewTwo = findViewById(R.id.canvasStreamsTextViewTwo);
        TextView canvasStreamsTextViewThree = findViewById(R.id.canvasStreamsTextViewThree);
        TextView canvasStreamsTextViewFour = findViewById(R.id.canvasStreamsTextViewFour);
        try {
            canvasRevenueStreamLean = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("leanRevenue", ObjectSerializer.serialize(new ArrayList<String>())));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (canvasRevenueStreamLean != null && !canvasRevenueStreamLean.isEmpty()){
            canvasStreamsTextViewOne.setText(canvasRevenueStreamLean.get(0));
            canvasStreamsTextViewTwo.setText(canvasRevenueStreamLean.get(1));
            canvasStreamsTextViewThree.setText(canvasRevenueStreamLean.get(2));
            canvasStreamsTextViewFour.setText(canvasRevenueStreamLean.get(3));
        }
        else {
            canvasStreamsTextViewOne.setText("Tap to add Revenue Streams");
            canvasStreamsTextViewTwo.setText(" ");
            canvasStreamsTextViewThree.setText(" ");
            canvasStreamsTextViewFour.setText(" ");
        }
///////////////////////////////////////// REVENUE STREAMS ////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////// KEY METRICS ////////////////////////////////////////////////////
        Button leanKeyMetrics = findViewById(R.id.leanKeyMetrics);
        leanKeyMetrics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(leanCanvas.this, leanPopUpKeyMetrics.class);
                startActivity(intent);
            }
        });
        TextView canvasResourcesTextViewOne = findViewById(R.id.canvasResourcesTextViewOne);
        TextView canvasResourcesTextViewTwo = findViewById(R.id.canvasResourcesTextViewTwo);
        TextView canvasResourcesTextViewThree = findViewById(R.id.canvasResourcesTextViewThree);
        TextView canvasResourcesTextViewFour = findViewById(R.id.canvasResourcesTextViewFour);
        try {
            canvasKeyMetricsLean = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("leanKeyMetrics", ObjectSerializer.serialize(new ArrayList<String>())));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (canvasKeyMetricsLean != null && !canvasKeyMetricsLean.isEmpty()){
            canvasResourcesTextViewOne.setText(canvasKeyMetricsLean.get(0));
            canvasResourcesTextViewTwo.setText(canvasKeyMetricsLean.get(1));
            canvasResourcesTextViewThree.setText(canvasKeyMetricsLean.get(2));
            canvasResourcesTextViewFour.setText(canvasKeyMetricsLean.get(3));
        }
        else {
            canvasResourcesTextViewOne.setText("Tap to add Key Resources");
            canvasResourcesTextViewTwo.setText(" ");
            canvasResourcesTextViewThree.setText(" ");
            canvasResourcesTextViewFour.setText(" ");
        }
///////////////////////////////////////// KEY METRICS ////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////// SOLUTION ////////////////////////////////////////////////////
        Button leanSolution = findViewById(R.id.leanSolution);
        leanSolution.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(leanCanvas.this, leanPopUpSolution.class);
                startActivity(intent);
            }
        });
        TextView canvasActivitiesTextViewOne = findViewById(R.id.canvasActivitiesTextViewOne);
        TextView canvasActivitiesTextViewTwo = findViewById(R.id.canvasActivitiesTextViewTwo);
        TextView canvasActivitiesTextViewThree = findViewById(R.id.canvasActivitiesTextViewThree);
        TextView canvasActivitiesTextViewFour = findViewById(R.id.canvasActivitiesTextViewFour);
        try {
            canvasSolutionLean = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("leanSolution", ObjectSerializer.serialize(new ArrayList<String>())));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (canvasSolutionLean != null && !canvasSolutionLean.isEmpty()){
            canvasActivitiesTextViewOne.setText(canvasSolutionLean.get(0));
            canvasActivitiesTextViewTwo.setText(canvasSolutionLean.get(1));
            canvasActivitiesTextViewThree.setText(canvasSolutionLean.get(2));
            canvasActivitiesTextViewFour.setText(canvasSolutionLean.get(3));
        }
        else {
            canvasActivitiesTextViewOne.setText("Tap to add Key Activities");
            canvasActivitiesTextViewTwo.setText(" ");
            canvasActivitiesTextViewThree.setText(" ");
            canvasActivitiesTextViewFour.setText(" ");
        }
///////////////////////////////////////// SOLUTION ////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////PROBLEM////////////////////////////////////////////////////
        Button leanProblem = findViewById(R.id.leanProblem);
        leanProblem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(leanCanvas.this, leanPopUpProblem.class);
                startActivity(intent);
            }
        });

        TextView canvasPartnersTextViewOne = findViewById(R.id.canvasPartnersTextViewOne);
        TextView canvasPartnersTextViewTwo = findViewById(R.id.canvasPartnersTextViewTwo);
        TextView canvasPartnersTextViewThree = findViewById(R.id.canvasPartnersTextViewThree);
        TextView canvasPartnersTextViewFour = findViewById(R.id.canvasPartnersTextViewFour);
        TextView canvasPartnersTextViewFive = findViewById(R.id.canvasPartnersTextViewFive);
        try {
            canvasProblemLean = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("leanProblem", ObjectSerializer.serialize(new ArrayList<String>())));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (canvasProblemLean != null && !canvasProblemLean.isEmpty()){
            canvasPartnersTextViewOne.setText(canvasProblemLean.get(0));
            canvasPartnersTextViewTwo.setText(canvasProblemLean.get(1));
            canvasPartnersTextViewThree.setText(canvasProblemLean.get(2));
            canvasPartnersTextViewFour.setText(canvasProblemLean.get(3));
            canvasPartnersTextViewFive.setText(canvasProblemLean.get(4));
        } else {
            canvasPartnersTextViewOne.setText("Tap to add Key Partners");
            canvasPartnersTextViewTwo.setText(" ");
            canvasPartnersTextViewThree.setText(" ");
            canvasPartnersTextViewFour.setText(" ");
            canvasPartnersTextViewFive.setText(" ");
        }
////////////////////////////////////PROBLEM ////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //        ////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////COST STRUCTURE ////////////////////////////////////////////////////
        Button leanCost = findViewById(R.id.leanCost);
        leanCost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(leanCanvas.this, leanPopUpCostStructure.class);  //cambiar a channels
                startActivity(intent);
            }
        });
        TextView canvasCostTextViewOne = findViewById(R.id.canvasCostTextViewOne);
        TextView canvasCostTextViewTwo = findViewById(R.id.canvasCostTextViewTwo);
        TextView canvasCostTextViewThree = findViewById(R.id.canvasCostTextViewThree);
        TextView canvasCostTextViewFour = findViewById(R.id.canvasCostTextViewFour);
        try {
            canvasCostStructureLean = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("leanCostStructure", ObjectSerializer.serialize(new ArrayList<String>())));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (canvasCostStructureLean != null && !canvasCostStructureLean.isEmpty()){
            canvasCostTextViewOne.setText(canvasCostStructureLean.get(0));
            canvasCostTextViewTwo.setText(canvasCostStructureLean.get(1));
            canvasCostTextViewThree.setText(canvasCostStructureLean.get(2));
            canvasCostTextViewFour.setText(canvasCostStructureLean.get(3));
        }
        else {
            canvasCostTextViewOne.setText("Tap to add Cost Structure");
            canvasCostTextViewTwo.setText(" ");
            canvasCostTextViewThree.setText(" ");
            canvasCostTextViewFour.setText(" ");
        }
///////////////////////////////////////// COST STRUTURE////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    }
}
