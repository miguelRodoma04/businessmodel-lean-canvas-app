package com.example.miguel.businessmodelcanvas;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

/**
 * Created by MIGUEL on 28/02/2018.
 */

public class leanPopUpSolution extends Activity {

    ArrayList<String> activities = new ArrayList<>(); //Array de items de channels
    ArrayList<String> canvasActivities = new ArrayList<>(); //Array de items de channels en canvas
    EditText activitiesEditTextOne;
    EditText activitiesEditTextTwo;
    EditText activitiesEditTextThree;
    EditText activitiesEditTextFour;

    SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
        setContentView(R.layout.lean_popup_solution);

        sharedPreferences = this.getSharedPreferences("com.example.miguel.businessmodelcanvas", Context.MODE_PRIVATE);
        activitiesEditTextOne = findViewById(R.id.editTextActivities1);
        activitiesEditTextTwo = findViewById(R.id.editTextActivities2);
        activitiesEditTextThree = findViewById(R.id.editTextActivities3);
        activitiesEditTextFour = findViewById(R.id.editTextActivities4);

        popUpMetrics();  //metricas para que la Activity sea pop-up window


        final Button saveActivities = findViewById(R.id.saveActivities);
        saveActivities.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                activities.add(activitiesEditTextOne.getText().toString());
                activities.add(activitiesEditTextTwo.getText().toString());
                activities.add(activitiesEditTextThree.getText().toString());
                activities.add(activitiesEditTextFour.getText().toString());

                ////////////////////////ESTE ES EL BUENO/////////////////////////////////
                try {
                    sharedPreferences.edit().putString("leanSolution", ObjectSerializer.serialize(activities)).apply();  //Shared preferences ya guarda el arreglo y es posible obtenerlo si no se modifica los customerSegments EditTexts//////////
                    Log.i("first array ", activities.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }



               //Esto va en la Activity de classicCanvas para obtener lo que se guardó en el popUp///
                try {
                    canvasActivities = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("leanSolution", ObjectSerializer.serialize(new ArrayList<String>())));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Intent intent = new Intent(leanPopUpSolution.this, leanCanvas.class);
                startActivity(intent);
            }
        });

        final Button resetActivities = findViewById(R.id.resetActivities);
        resetActivities.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AlertDialog.Builder(leanPopUpSolution.this)
                        .setIcon(android.R.drawable.ic_dialog_dialer)
                        .setTitle("Reset Activities?")
                        .setMessage("Are you sure you want to reset all items?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                activities.add(null);
                                activities.add(null);
                                activities.add(null);
                                activities.add(null);

                                ////////////////////////ESTE ES EL BUENO/////////////////////////////////
                                try {
                                    sharedPreferences.edit().putString("leanSolution", ObjectSerializer.serialize(activities)).apply();  //Shared preferences ya guarda el arreglo y es posible obtenerlo si no se modifica los customerSegments EditTexts//////////
                                    Log.i("first array ", activities.toString());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                //Esto va en la Activity de classicCanvas para obtener lo que se guardó en el popUp///
                                try {
                                    canvasActivities = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("leanSolution", ObjectSerializer.serialize(new ArrayList<String>())));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                activitiesEditTextOne.setText(null);
                                activitiesEditTextTwo.setText(null);
                                activitiesEditTextThree.setText(null);
                                activitiesEditTextFour.setText(null);

                                Intent intent = new Intent(leanPopUpSolution.this, leanCanvas.class);
                                startActivity(intent);   ///ver si se puede hacer sin el intent//
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();
            }
        });

        //Esto va en la Activity de classicCanvas para obtener lo que se guardó en el popUp///
        try {
            canvasActivities = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("leanSolution", ObjectSerializer.serialize(new ArrayList<String>())));
        } catch (Exception e) {
            e.printStackTrace();
        }
         ///////////////
        if (canvasActivities != null && !canvasActivities.isEmpty()){
            if(canvasActivities.get(0) != null) {
                activitiesEditTextOne.setText(canvasActivities.get(0));
            }
            if(canvasActivities.get(1) != null) {
                activitiesEditTextTwo.setText(canvasActivities.get(1));
            }
            if(canvasActivities.get(2) != null) {
                activitiesEditTextThree.setText(canvasActivities.get(2));
            }
            if(canvasActivities.get(3) != null) {
                activitiesEditTextFour.setText(canvasActivities.get(3));
            }
        }
    }

    public void popUpMetrics(){
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        getWindow().setLayout((int)(width*0.7),(int)(height*0.7));
    }
}

