package com.example.miguel.businessmodelcanvas;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

/**
 * Created by MIGUEL on 28/02/2018.
 */

public class popUpCost extends Activity {

    ArrayList<String> cost = new ArrayList<>(); //Array de items de channels
    ArrayList<String> canvasCost = new ArrayList<>(); //Array de items de channels en canvas
    EditText costEditTextOne;
    EditText costEditTextTwo;
    EditText costEditTextThree;
    EditText costEditTextFour;

    SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
        setContentView(R.layout.pop_up_cost_structure);

        sharedPreferences = this.getSharedPreferences("com.example.miguel.businessmodelcanvas", Context.MODE_PRIVATE);
        costEditTextOne = findViewById(R.id.editTextCost1);
        costEditTextTwo = findViewById(R.id.editTextCost2);
        costEditTextThree = findViewById(R.id.editTextCost3);
        costEditTextFour = findViewById(R.id.editTextCost4);

        popUpMetrics();  //metricas para que la Activity sea pop-up window


        final Button saveCost = findViewById(R.id.saveCost);
        saveCost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                cost.add(costEditTextOne.getText().toString());
                cost.add(costEditTextTwo.getText().toString());
                cost.add(costEditTextThree.getText().toString());
                cost.add(costEditTextFour.getText().toString());

                ////////////////////////ESTE ES EL BUENO/////////////////////////////////
                try {
                    sharedPreferences.edit().putString("cost", ObjectSerializer.serialize(cost)).apply();  //Shared preferences ya guarda el arreglo y es posible obtenerlo si no se modifica los customerSegments EditTexts//////////
                    Log.i("first array ", cost.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }



               //Esto va en la Activity de classicCanvas para obtener lo que se guardó en el popUp///
                try {
                    canvasCost = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("cost", ObjectSerializer.serialize(new ArrayList<String>())));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Intent intent = new Intent(popUpCost.this, classicCanvas.class);
                startActivity(intent);
            }
        });

        final Button resetCost = findViewById(R.id.resetCost);
        resetCost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AlertDialog.Builder(popUpCost.this)
                        .setIcon(android.R.drawable.ic_dialog_dialer)
                        .setTitle("Reset COST Structure??")
                        .setMessage("Are you sure you want to reset all items?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                cost.add(null);
                                cost.add(null);
                                cost.add(null);
                                cost.add(null);

                                ////////////////////////ESTE ES EL BUENO/////////////////////////////////
                                try {
                                    sharedPreferences.edit().putString("cost", ObjectSerializer.serialize(cost)).apply();  //Shared preferences ya guarda el arreglo y es posible obtenerlo si no se modifica los customerSegments EditTexts//////////
                                    Log.i("first array ", cost.toString());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                //Esto va en la Activity de classicCanvas para obtener lo que se guardó en el popUp///
                                try {
                                    canvasCost = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("cost", ObjectSerializer.serialize(new ArrayList<String>())));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                costEditTextOne.setText(null);
                                costEditTextTwo.setText(null);
                                costEditTextThree.setText(null);
                                costEditTextFour.setText(null);

                                Intent intent = new Intent(popUpCost.this, classicCanvas.class);
                                startActivity(intent);   ///ver si se puede hacer sin el intent//
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();
            }
        });

        //Esto va en la Activity de classicCanvas para obtener lo que se guardó en el popUp///
        try {
            canvasCost = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("cost", ObjectSerializer.serialize(new ArrayList<String>())));
        } catch (Exception e) {
            e.printStackTrace();
        }
         ///////////////
        if (canvasCost != null && !canvasCost.isEmpty()){
            if(canvasCost.get(0) != null) {
                costEditTextOne.setText(canvasCost.get(0));
            }
            if(canvasCost.get(1) != null) {
                costEditTextTwo.setText(canvasCost.get(1));
            }
            if(canvasCost.get(2) != null) {
                costEditTextThree.setText(canvasCost.get(2));
            }
            if(canvasCost.get(3) != null) {
                costEditTextFour.setText(canvasCost.get(3));
            }
        }
    }

    public void popUpMetrics(){
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        getWindow().setLayout((int)(width*0.7),(int)(height*0.7));
    }
}

