package com.example.miguel.businessmodelcanvas;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

/**
 * Created by MIGUEL on 28/02/2018.
 */

public class popUpRelationships extends Activity {

    ArrayList<String> relationships = new ArrayList<>(); //Array de items de channels
    ArrayList<String> canvasRelationships = new ArrayList<>(); //Array de items de channels en canvas
    EditText relationshipsEditTextOne;
    EditText relationshipsEditTextTwo;
    EditText relationshipsEditTextThree;
    EditText relationshipsEditTextFour;

    SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
        setContentView(R.layout.pop_up_relationships);

        sharedPreferences = this.getSharedPreferences("com.example.miguel.businessmodelcanvas", Context.MODE_PRIVATE);
        relationshipsEditTextOne = findViewById(R.id.editTextRelationships1);
        relationshipsEditTextTwo = findViewById(R.id.editTextRelationships2);
        relationshipsEditTextThree = findViewById(R.id.editTextRelationships3);
        relationshipsEditTextFour = findViewById(R.id.editTextRelationships4);

        popUpMetrics();  //metricas para que la Activity sea pop-up window


        final Button saveRelationships = findViewById(R.id.saveRelationships);
        saveRelationships.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                relationships.add(relationshipsEditTextOne.getText().toString());
                relationships.add(relationshipsEditTextTwo.getText().toString());
                relationships.add(relationshipsEditTextThree.getText().toString());
                relationships.add(relationshipsEditTextFour.getText().toString());

                ////////////////////////ESTE ES EL BUENO/////////////////////////////////
                try {
                    sharedPreferences.edit().putString("relationships", ObjectSerializer.serialize(relationships)).apply();  //Shared preferences ya guarda el arreglo y es posible obtenerlo si no se modifica los customerSegments EditTexts//////////
                    Log.i("first array ", relationships.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }



               //Esto va en la Activity de classicCanvas para obtener lo que se guardó en el popUp///
                try {
                    canvasRelationships = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("relationships", ObjectSerializer.serialize(new ArrayList<String>())));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Intent intent = new Intent(popUpRelationships.this, classicCanvas.class);
                startActivity(intent);
            }
        });

        final Button resetRelationships = findViewById(R.id.resetRelationships);
        resetRelationships.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AlertDialog.Builder(popUpRelationships.this)
                        .setIcon(android.R.drawable.ic_dialog_dialer)
                        .setTitle("Reset Channels?")
                        .setMessage("Are you sure you want to reset all items?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                relationships.add(null);
                                relationships.add(null);
                                relationships.add(null);
                                relationships.add(null);

                                ////////////////////////ESTE ES EL BUENO/////////////////////////////////
                                try {
                                    sharedPreferences.edit().putString("relationships", ObjectSerializer.serialize(relationships)).apply();  //Shared preferences ya guarda el arreglo y es posible obtenerlo si no se modifica los customerSegments EditTexts//////////
                                    Log.i("first array ", relationships.toString());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                //Esto va en la Activity de classicCanvas para obtener lo que se guardó en el popUp///
                                try {
                                    canvasRelationships = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("relationships", ObjectSerializer.serialize(new ArrayList<String>())));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                relationshipsEditTextOne.setText(null);
                                relationshipsEditTextTwo.setText(null);
                                relationshipsEditTextThree.setText(null);
                                relationshipsEditTextFour.setText(null);

                                Intent intent = new Intent(popUpRelationships.this, classicCanvas.class);
                                startActivity(intent);   ///ver si se puede hacer sin el intent//
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();
            }
        });

        //Esto va en la Activity de classicCanvas para obtener lo que se guardó en el popUp///
        try {
            canvasRelationships = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("relationships", ObjectSerializer.serialize(new ArrayList<String>())));
        } catch (Exception e) {
            e.printStackTrace();
        }
         ///////////////
        if (canvasRelationships != null && !canvasRelationships.isEmpty()){
            if(canvasRelationships.get(0) != null) {
                relationshipsEditTextOne.setText(canvasRelationships.get(0));
            }
            if(canvasRelationships.get(1) != null) {
                relationshipsEditTextTwo.setText(canvasRelationships.get(1));
            }
            if(canvasRelationships.get(2) != null) {
                relationshipsEditTextThree.setText(canvasRelationships.get(2));
            }
            if(canvasRelationships.get(3) != null) {
                relationshipsEditTextFour.setText(canvasRelationships.get(3));
            }
        }
    }

    public void popUpMetrics(){
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        getWindow().setLayout((int)(width*0.7),(int)(height*0.7));
    }
}

