package com.example.miguel.businessmodelcanvas;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by MIGUEL on 28/02/2018.
 */

public class popUpCustomerSegments extends Activity {

    ArrayList<String> customerSegments = new ArrayList<>();   //Arreglo donde se guardan los 5 customer segments
    ArrayList<String> canvasCustomerSegments = new ArrayList<>();//Arreglo donde se guardan items que van en canvas
    EditText customerSegmentEditTextOne;
    EditText customerSegmentEditTextTwo;
    EditText customerSegmentEditTextThree;
    EditText customerSegmentEditTextFour;
    EditText customerSegmentEditTextFive;

    SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
        setContentView(R.layout.pop_up_customer_segments);

        sharedPreferences = this.getSharedPreferences("com.example.miguel.businessmodelcanvas", Context.MODE_PRIVATE);
        customerSegmentEditTextOne = findViewById(R.id.editTextCustSegm1);
        customerSegmentEditTextTwo = findViewById(R.id.editTextCustSegm2);
        customerSegmentEditTextThree = findViewById(R.id.editTextCustSegm3);
        customerSegmentEditTextFour = findViewById(R.id.editTextCustSegm4);
        customerSegmentEditTextFive = findViewById(R.id.editTextCustSegm5);

        popUpMetrics();  //metricas para que la Activity sea pop-up window}


        final Button saveCustomerSegment = findViewById(R.id.saveCustomerSegment);
        saveCustomerSegment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                customerSegments.add(customerSegmentEditTextOne.getText().toString());
                customerSegments.add(customerSegmentEditTextTwo.getText().toString());
                customerSegments.add(customerSegmentEditTextThree.getText().toString());
                customerSegments.add(customerSegmentEditTextFour.getText().toString());
                customerSegments.add(customerSegmentEditTextFive.getText().toString());



                ////////////////////////ESTE ES EL BUENO/////////////////////////////////
                try {
                    sharedPreferences.edit().putString("segments", ObjectSerializer.serialize(customerSegments)).apply();  //Shared preferences ya guarda el arreglo y es posible obtenerlo si no se modifica los customerSegments EditTexts//////////
                    Log.i("first array ", customerSegments.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }

               //Esto va en la Activity de classicCanvas para obtener lo que se guardó en el popUp///
                try {
                    canvasCustomerSegments = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("segments", ObjectSerializer.serialize(new ArrayList<String>())));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Intent intent = new Intent(popUpCustomerSegments.this, classicCanvas.class);
                startActivity(intent);
            }
        });

        //////reset customer segment/////
        final Button resetCustomerSegment = findViewById(R.id.resetCustomerSegment);
        resetCustomerSegment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AlertDialog.Builder(popUpCustomerSegments.this)
                        .setIcon(android.R.drawable.ic_dialog_dialer)
                        .setTitle("Reset Customer Segments")
                        .setMessage("Are you sure you want to reset all items?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                customerSegments.add(null);
                                customerSegments.add(null);
                                customerSegments.add(null);
                                customerSegments.add(null);
                                customerSegments.add(null);

                                ////////////////////////ESTE ES EL BUENO/////////////////////////////////
                                try {
                                    sharedPreferences.edit().putString("segments", ObjectSerializer.serialize(customerSegments)).apply();  //Shared preferences ya guarda el arreglo y es posible obtenerlo si no se modifica los customerSegments EditTexts//////////
                                    Log.i("first array ", customerSegments.toString());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                //Esto va en la Activity de classicCanvas para obtener lo que se guardó en el popUp///
                                try {
                                    canvasCustomerSegments = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("segments", ObjectSerializer.serialize(new ArrayList<String>())));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                customerSegmentEditTextOne.setText(null);
                                customerSegmentEditTextTwo.setText(null);
                                customerSegmentEditTextThree.setText(null);
                                customerSegmentEditTextFour.setText(null);
                                customerSegmentEditTextFive.setText(null);

                                Intent intent = new Intent(popUpCustomerSegments.this, classicCanvas.class);
                                startActivity(intent);   ///ver si se puede hacer sin el intent//
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();
            }
        });

        //Esto va en la Activity de classicCanvas para obtener lo que se guardó en el popUp///
        try {
            canvasCustomerSegments = (ArrayList<String>) ObjectSerializer.deserialize(sharedPreferences.getString("segments", ObjectSerializer.serialize(new ArrayList<String>())));
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (canvasCustomerSegments != null && !canvasCustomerSegments.isEmpty()){
            if(canvasCustomerSegments.get(0) != null) {
                customerSegmentEditTextOne.setText(canvasCustomerSegments.get(0));
            }
            if(canvasCustomerSegments.get(1) != null) {
                customerSegmentEditTextTwo.setText(canvasCustomerSegments.get(1));
            }
            if(canvasCustomerSegments.get(2) != null) {
                customerSegmentEditTextThree.setText(canvasCustomerSegments.get(2));
            }
            if(canvasCustomerSegments.get(3) != null) {
                customerSegmentEditTextFour.setText(canvasCustomerSegments.get(3));
            }
            if(canvasCustomerSegments.get(4) != null) {
                customerSegmentEditTextFive.setText(canvasCustomerSegments.get(4));
            }
        }
    }




    public void popUpMetrics(){
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        getWindow().setLayout((int)(width*0.7),(int)(height*0.7));
    }
}

